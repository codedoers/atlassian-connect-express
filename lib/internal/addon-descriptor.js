var utils = require('./utils');

module.exports = function (addon) {
    return addon.config.descriptorFactory()();
};
